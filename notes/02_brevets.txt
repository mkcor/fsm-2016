J'aimerais maintenant aborder le problème des brevets logiciels. Pourquoi? Le
slogan de l'April, c'est "promouvoir et défendre le logiciel libre". Jusqu'à
présent, je vous ai parlé d'émancipation, de liberté, de respect, de
possibilités, donc de choses positives, qui correspondent au volet
"promouvoir". Alors pourquoi avons-nous ce pendant "négatif"? Contre quoi
doit-on se défendre? Je pense que les brevets logiciels constituent la plus
grande menace envers le logiciel libre. En fait, ça va bien au-delà, les
brevets logiciels menacent le développement logiciel dans son ensemble, c'est
pourquoi je vais me hasarder plus tard à parler d'innovation, même si c'est
un terme fourre-tout (que donc l'April a tendance à éviter). Pour l'anecdote,
les brevets logiciels tiennent une place particulière dans mon parcours
militant personnel : mon premier combat pour défendre le logiciel libre a
été contre le projet de brevet unitaire européen, en 2012 (j'étais basée
en France à l'époque). Je précise que ce projet, au niveau européen donc,
partait d'une bonne idée mais, malheureusement, cela a été l'occasion pour
différents lobbies (qui ne sont jamais très loin) de pousser pour un système
des brevets à l'américaine, dont je vais vous décrire certains aspects.

Je ne vais pas parler de la législation sur les brevets en général (ce n'est
pas le sujet), mais du cas précis où elle est appliquée au domaine du
logiciel. Cela permet de voir, conceptuellement, qu'avant même de parler de
"brevets logiciels", on peut (doit) s'interroger sur la *brevetabilité* du
logiciel : est-ce pertinent, est-ce légitime d'appliquer la législation sur
les brevets au domaine du logiciel? La réponse est non de mon point de vue,
bien sûr, et du point de vue de l'April, de la FSF. Mais pas seulement! C'est
aussi une conclusion qui atteint les plus hauts niveaux de gouvernement, donc
certaines juridictions ont décidé avec raison que le logiciel n'était pas
brevetable. Je pense à la Nouvelle-Zélande, qui est exemplaire à ce niveau,
et à l'Union Européenne où, *en principe*, les brevets logiciels sont aussi
interdits. En pratique, la brevetabilité (ou non) des logiciels est souvent
définie par la jurisprudence, puisque les logiciels n'existaient pas quand les
textes de loi originaux ont été écrits. C'est pourquoi cette question de la
brevetabilité des logiciels est politique et pas seulement juridique. Et c'est
pourquoi nous sommes rassemblés ici (je reviendrai là-dessus dans ma
conclusion).

J'imagine que, même sans être des experts du droit (et je n'en suis pas une
non plus), vous avez entendu parler des brevets, comme étant censés protéger
des inventions (technologiques). Donc un "inventeur" peut déposer une demande
de brevet auprès d'un état ou d'un groupe d'états. Aux États-Unis, ce sera
auprès du USPTO (United States Patent and Trademark Office), en Europe, ce
sera l'OEB (Office Européen des Brevets), qui couvre 38 états européens, au
Canada, c'est l'Office de la Propriété Intellectuelle du Canada (OPIC), qui
est une agence fédérale (qui fait partie de Innovation, Sciences et
Développement économique Canada). Alors, il faut commencer par quelques mises
en garde. Déjà, le terme "propriété intellectuelle", j'ai été obligée de
le prononcer à l'instant, pour citer l'agence fédérale en question, mais je
le rejette (je pense que c'est un oxymore). À l'April, à la FSF, nous
dénonçons ce terme partial, vague, et qui prête à confusion. Les brevets
d'un côté, les marques commerciales, et le droit d'auteur n'ont rien à voir
entre eux, donc si l'on veut appréhender ces questions, on doit d'abord les
séparer, les distinguer. Je rappelle que je ne traite ici que des brevets.

Petite parenthèse, je suis dans l'informatique professionnellement (dans la
"tech"), pendant trois ans j'ai travaillé dans des startups. Je garde certains
liens avec le monde universitaire, la recherche. Je fais maintenant de la
recherche très appliquée, très industrielle. J'ai l'impression que c'est
systématiquement auprès de l'administration *américaine* que les demandes de
brevets sont déposées, en tout cas dans les milieux que je fréquente ici au
Québec, au Canada. Détenir un brevet canadien ne semble pas une
préoccupation... Ce qui se comprend, puisqu'il s'agit d'obtenir un droit
exclusif d'exploitation sur un territoire donné, or le marché américain est
incommensurablement plus intéressant (enfin, plus grand) que notre marché
intérieur. Je ferme plus ou moins la parenthèse et je reviens aux mises en
garde.

Alors tu déposes une demande de brevet, et l'agence de délivrance des brevets
doit évaluer si ton "invention" est bel et bien une invention ("nouvelle,
utile et non évidente" selon la formulation américaine). Cette évaluation
est-elle faite de façon neutre, impartiale et de façon compétente,
éclairée? Intuitivement, vous comprenez que si "brevet == innovation ==
bien", mettons que c'est le discours gouvernemental (ou même l'opinion
publique, ou l'idéologie dominante) et qu'il s'agit d'une agence
gouvernementale, alors on a envie de délivrer des brevets, plus de brevets,
toujours plus de brevets. Le cas de l'OEB est encore plus clair, encore plus
gros, car ce n'est pas une agence gouvernementale, mais une organisation
autonome -- financièrement. Elle génère donc ses propres revenus, et ces
revenus proviennent uniquement des demandes de brevets. Vous devinez alors la
tendance, les demandeurs de brevets sont les clients de l'OEB, l'OEB a tout
intérêt à plaire à ses clients (en acceptant les demandes, en délivrant
les brevets) et à augmenter sa clientèle. On se retrouve avec une
organisation qui est juge et partie.

Rendus là, nous commençons à percevoir les contours de ce que nous appelons
à l'April le "microcosme des brevets". Le microcosme des brevets est formé par
tous les acteurs qui ont un intérêt dans le système des brevets, qui parfois
n'existent que par et pour le système des brevets. On retrouve parmi ces
acteurs les cabinets d'avocats spécialisés dans le droit des brevets, les
agences de délivrance donc, et les très grosses entreprises, qui ont les
moyens de s'approprier des marchés, de s'arranger entre elles en échangeant
des portefeuilles de brevets, etc. On ne retrouve certainement pas le petit
génie qui ferait des découvertes dans son sous-sol, ça n'existe pas (ce
n'est pas de même qu'on travaille en développement logiciel). On ne retrouve
pas non plus les jeunes pousses (startups) ou les petites entreprises, qui
n'ont pas à leur disposition des armées de juristes spécialisés dans les
brevets. Or quand on cherche des informations ou des opinions sur le système
des brevets, c'est typiquement le microcosme des brevets qui va s'exprimer, à
qui l'on va tendre le micro, parce que ce sont eux qui connaissent le système
des brevets, puisque ce sont eux qui le construisent, puisque ce sont eux qui
en vivent. Voilà la raison de ma mise en garde.

Donc il y a un problème, je pense, d'autoréférencement idéologique mais
aussi, très concrètement, de pouvoir financier. Vous avez peut-être entendu
parler des guerres de brevets, ces séries de procès à coups de millions de
dollars, quand une entreprise (pensez aux gros joueurs de la technologie)
estime que (a de bons avocats spécialisés pour prouver que) elle est victime
de violation de brevet, elle peut attaquer un concurrent et peut-être faire
plus d'argent ainsi qu'en sortant un bon produit -- vive l'innovation! Pour
éviter la guerre ouverte (incessante), les grosses sociétés échangent entre
elles des portefeuilles de brevets -- afin de contourner le système, le
système des brevets, qui empêche, qui gêne les activités de recherche et
développement. Encore une fois, les petits acteurs ne peuvent prendre part à
ce jeu. S'ils sont poursuivis pour violation de brevet, ils n'auront pas les
moyens de se défendre, peu importe qu'il y ait ou non violation de brevet.
Cela a conduit au développement d'un commerce, qui consiste à menacer les
gens de procès et faire payer les gens qui font réellement du développement.
C'est du racket!

Aux États-Unis, il y a des centaines de milliers de brevets logiciels. Plus on
continue de délivrer des brevets logiciels, plus ces activités de racket
peuvent elles se développer (mais pas le logiciel) -- plus des firmes
spécialisées peuvent menacer n'importe quel acteur technologique de
l'attaquer pour violation de brevet. Le microcosme des brevets est extrêmement
puissant, il peut se payer des lobbyistes professionnels chevronnés. Alors
quelle que soit votre définition de l'innovation, mettons que l'idéologie
dominante l'associe aux notions d'émergence, de compétition et de
concurrence : pensez-vous que le maintien des positions dominantes et
l'intimidation auprès des développeurs de logiciels (qu'ils soient libres ou
pas) aillent de pair avec l'innovation?

Ce sont les abus auxquels la brevetabilité du logiciel conduit. En fait, même
chez les grandes sociétés, il y a une problématique, car les coûts
juridiques ne font qu'augmenter dans les budgets de recherche et
développement. Vous voyez que la brevetabilité logicielle transcende
complètement le mouvement du logiciel libre, et nous pouvons faire des
alliances de circonstance... Revenons à la question initiale : pourquoi le
logiciel doit-il être exclu du champ de la brevetabilité? Parce qu'un
logiciel est l'implémentation d'un algorithme, d'une méthode, et qu'il se
construit incrémentalement, par petites briques. Tu ne réinventes pas la
route, tu reprends des implémentations existantes, tu ajoutes ta petite
brique, tu ne vas pas te demander avant de coder si quelqu'un a déjà eu la
même démarche, la même idée dans son cerveau, parce que tu as autre chose
à faire, tu veux avancer, et de toutes façons la réponse est sûrement
"oui", quelqu'un a déjà fait/pensé cela, car ça n'a rien de génial.

La brevetabilité du logiciel fait peser une menace financière et juridique
sur (tous) les développeurs de logiciels -- tu penses développer un logiciel
dans ton coin, mais tu pourrais devoir payer des redevances exorbitantes. La
brevetabilité logicielle va à l'encontre du foisonnement de la créativité
humaine et de l'activité économique. Par rapport à la dynamique des
logiciels libres, a fortiori, on a un antagonisme, une incompatibilité
absolue. Parce que les logiciels libres sont le résultat et le véhicule du
savoir humain, de son partage, de son évolution perpétuelle. Je voudrais
conclure sur l'importance et l'utilité de la sensibilisation, rappelez-vous,
j'ai mentionné la jurisprudence. Le droit et surtout l'application du droit
reflète aussi les forces politiques en présence. Le droit, ça s'interprête.
Si l'opinion publique, les élus, les gouvernements penchent contre la
brevetabilité du logiciel (comme je le souhaite), c'est un terrain favorable
pour que les décisions de justice aillent plutôt contre les brevets
logiciels. C'est ce que nous pouvons faire dans les juridictions qui ne
garantissent pas (encore) la non brevetabilité logicielle, et même dans
celles qui la garantissent, elle est toujours menacée, directement ou
indirectement, lorsque la souveraineté juridique est menacée, comme c'est le
cas typiquement dans les projets de libre-échange.

TAFTA/TTIP recyclait ACTA ; quel sera le prochain? Ces approches
libre-échangistes (qui reflètent l'idéologie mondiale dominante) constituent
des menaces à la fois directes et indirectes pour le logiciel libre. Comme
vous le savez (sans doute mieux que moi), nous (les citoyens, les militants)
nous mobilisons lors de campagnes. Mais les lobbyistes professionnels tels ceux
du microcosme des brevets travaillent à temps plein, à l'année longue. Ça
me fera plaisir d'échanger là-dessus avec vous.
