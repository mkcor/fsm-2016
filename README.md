# Forum Social Mondial 2016

[Grande conférence](https://fsm2016.org/activites/grande-conference-logiciel-libre/)
sur le logiciel libre organisée par [FACIL](https://facil.qc.ca/).

[Enregistrement vidéo](https://archive.org/details/FSM-2016-LogicielLibre) de
la conférence.

Intervention de [Marianne Corvellec](http://www.april.org/intervenants-april#Marianne).

## Notes

* `00_intro.txt`
* `01_emancipation.txt`
* `02_brevets.txt`
* `03_surveillance.txt`
